# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NewsItem(scrapy.Item):
    website= scrapy.Field()
    url= scrapy.Field()
    update_datetime= scrapy.Field()
    title= scrapy.Field()
    abstract = scrapy.Field()
    content= scrapy.Field()
    datetime= scrapy.Field()
    original= scrapy.Field()
    author=scrapy.Field()
