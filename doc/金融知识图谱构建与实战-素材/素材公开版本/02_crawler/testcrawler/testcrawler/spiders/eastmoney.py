# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
import re
import requests
from urllib import parse
from testcrawler.items import NewsItem
import time
from bs4 import BeautifulSoup


class EastmoneySpider(scrapy.Spider):
    name = 'eastmoney'
    allowed_domains = ['finance.eastmoney.com']
    start_urls = ['http://finance.eastmoney.com/']

    def parse(self, response):
        item = list()
        url_prefix = "http://finance.eastmoney.com/news/cgsxw"
        for i in range(25):
            if i == 0:
                url = url_prefix + ".html"
            else:
                url = url_prefix + "_" + str(i) + ".html"
            print(url)
            r = requests.get(url)
            content = r.text
            soup = BeautifulSoup(content, 'lxml')
            divs = soup.find_all(href=re.compile("http://finance.eastmoney.com/news/"))

            for div in divs:
                li = BeautifulSoup(str(div), 'lxml')
                d = li.a.get("href")
                if len(d)>46 and len(d)<66 :
                    item.append(d)
                    print(d)
                else:continue
        for i in item:
            time.sleep(1)
            yield Request(url=parse.urljoin(response.url, i), callback=self._parse_detail)

    @staticmethod
    def _parse_detail(response):
        item = NewsItem()
        requests.utils.get_encodings_from_content(response.text)
        html = response.text
        item['website'] = "东方财富网"
        item['update_datetime'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        item['url'] = response.url

        item['title'] = response.xpath("//h1/text()").extract()
        item['datetime'] = response.xpath("//div[@class='time']/text()").extract()
        original = response.xpath("//div[@class='source data-source']/text()").extract()
        item['original'] = ("".join(original)).replace("\r\n", "").replace("\r", "").replace("\n", "").\
            replace("\t", "").strip()
        author =  response.xpath("//p[@class='res-edit']/text()").extract()
        item['author'] = ("".join(author)).replace("\r\n", "").replace("\r", "").replace("\n", "").\
            replace("\t", "").strip()
        item['abstract'] = 'NULL'
        content = response.xpath(
            "//div[@id='ContentBody']/p/text()|//div[@id='ContentBody']/p/strong/text()").extract()
        item['content'] = ("".join(content)).replace("\r\n", "").replace("\r", "").replace("\n", "").\
            replace("\t", "").strip()

        print(item)
        yield item